#include "main_menu.h"

Scene* MainMenu::scene() {
    Scene *scene = Scene::node();
    MainMenu* layer = new MainMenu;
    scene->addChild(layer);
    return scene;
}


MainMenu::MainMenu()
{
    qDebug() << Q_FUNC_INFO;
    //taking screen size, saving it to variable called winSize
    m_winSize = Director::sharedDirector()->winSizeInPixels();

    //we'll start by adding a sprite that represents the background for the menu
    m_background = Sprite::spriteWithImage("wallpaper.jpg");
    m_background->setPos(m_winSize.width()/2,m_winSize.height()/2);
    addChild(m_background);
    QString fileName;
    QString temp;
    Sprite *newCard;
    qreal offset = 100;
    //we'll creat the 13 club cards
    fileName = "c.png";
    for (int i = 1; i <= 13; i++) {
        temp = QString().setNum(i);
        temp.append(fileName);
        newCard = Sprite::spriteWithImage(temp.toAscii());
        m_cards.append(newCard);
        newCard->setPos(offset,offset);
        addChild(newCard);
    }
    //we'll creat the 13 diamond cards
    fileName = "d.png";
    for (int i = 1; i <= 13; i++) {
        temp = QString().setNum(i);
        temp.append(fileName);
        newCard = Sprite::spriteWithImage(temp.toAscii());
        m_cards.append(newCard);
        newCard->setPos(offset,offset);
        addChild(newCard);
    }
    //we'll creat the 13 heart cards
    fileName = "h.png";
    for (int i = 1; i <= 13; i++) {
        temp = QString().setNum(i);
        temp.append(fileName);
        newCard = Sprite::spriteWithImage(temp.toAscii());
        m_cards.append(newCard);
        newCard->setPos(offset,offset);
        addChild(newCard);
    }
    //we'll creat the 13 Spade cards
    fileName = "s.png";
    for (int i = 1; i <= 13; i++) {
        temp = QString().setNum(i);
        temp.append(fileName);
        newCard = Sprite::spriteWithImage(temp.toAscii());
        m_cards.append(newCard);
        newCard->setPos(offset,offset);
        addChild(newCard);
    }
    m_counter = 0;
    animation();
}

void MainMenu::animation() {
    if (m_counter < m_cards.size()) {
        m_cards.at(m_counter)->runSequence(Sequence::sequenceWithActions(Action::easeinout(Action::moveBy(0.5,300,200)),Action::FuncCall(this,SLOT(animation())),NULL));
        m_cards.at(m_counter)->runAction(Action::easeinout(Action::rotateBy(0.5,360)));
        AudioManager::playSound("alert.wav");
        AudioManager::playSound("play_card.wav");
        m_counter++;
    }
}

void MainMenu::update(double delta) {
//    runAction(action::rotateBy(4,360));
    runAction(Action::easeinout(Action::scaleTo(1,1)));
    runAction(Action::fadeTo(1,100));
    unSchedualUpdate();
}


void MainMenu::onEnterTransitionDidStart() {
    qDebug() << Q_FUNC_INFO;
}

void MainMenu::onEnterTransitionDidFinish() {
    qDebug() << Q_FUNC_INFO;
}

void MainMenu::onExitTransitionDidStart() {
    qDebug() << Q_FUNC_INFO;
}

void MainMenu::onExitTransitionDidFinish() {
    qDebug() << Q_FUNC_INFO;
}

