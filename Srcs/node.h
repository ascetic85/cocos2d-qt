#ifndef NODE_H
#define NODE_H
#include "QPointF"
#include "QGraphicsScene"
#include <QGraphicsItem>
#include "sequence.h"
#include "director.h"
#include "trace.h"

class Action;
class Sequence;

class Node : public QObject
{
    Q_OBJECT
    Q_PROPERTY (qreal scale READ scale WRITE setScale)
    Q_PROPERTY (qreal opacity READ opacity WRITE setOpacity)
    Q_PROPERTY (qreal rotate READ rotation WRITE setRotation)
    Q_PROPERTY (QPointF pos READ pos WRITE setPos)
public:
    //these functions should be implemented in the subclass
    virtual qreal getwidth() = 0;
    virtual qreal getheight() = 0;
    virtual QPointF pos() const;
    virtual qreal opacity() const;
    virtual qreal scale();
    virtual qreal rotation();
    virtual int z();
    virtual void setOpacity(qreal value);
    virtual void setPos ( const QPointF & pos );
    virtual void setPos ( qreal x, qreal y );
    virtual void setScale ( qreal factor );
    virtual void setZ ( int zValue );
    virtual void setRotation ( qreal rotationDegree );
    virtual Action* runAction(Action* actionPara);
    virtual void runSequence(Sequence* sequencePara);
    virtual void stopAction(Action* stopAction);
    virtual void stopAllActions();
    void schedualUpdate();
    void unSchedualUpdate();
    qreal width();
    qreal height();
    ~Node();
    Node();
    virtual void addChild(Node* child,int tag = 0,int z = 0);
    virtual Node* getChildByTag(int tag);
    virtual void removeChildByTag(int tag);
    virtual void removeChild(Node* child);
    virtual void onEnterTransitionDidStart(){}
    virtual void onEnterTransitionDidFinish(){}
    virtual void onExitTransitionDidStart(){}
    virtual void onExitTransitionDidFinish(){}
    virtual void touchBegin(QGraphicsSceneMouseEvent /**event*/) {}
    virtual void touchMove(QGraphicsSceneMouseEvent /**event*/) {}
    virtual void touchEnd(QGraphicsSceneMouseEvent /**event*/) {}
    void setTouchEnabled(bool enabled = true);

public slots:
    void actionDone(Action* doneAction);
    virtual void update(double delta);

public slots:
    void updateMyPos();
    void updateMyScale();
    void updateMyZ();
    void updateMyRotation();
    void updateMyOpacity();

signals:
    void updateChildsPos();
    void updateChildsScale();
    void updateChildsZ();
    void updateChildsRotation();
    void updateChildsOpacity();

protected:
    // these functions are overloaded in base classes as private with no implementation, because they need to be protected
    virtual QGraphicsScene* getSharedGraphicScene();
    virtual void setSharedGraphicScene(QGraphicsScene* graphicscene);
    virtual int getGlobalMaxZ();
    virtual QGraphicsItem* getGraphicsItem();
    virtual void setGraphicsItem(QGraphicsItem* item);
    virtual void setW(qreal m_width);
    virtual void setH(qreal m_height);
    // end of protected members

private:
    QPointF     realTimePos() const;
    qreal     realTimeScale() const;
    int     realTimeZ() const;
    qreal     realTimeRotation() const;
    qreal     realTimeOpacity() const;

private:
    bool m_updatescheduled;
    static QGraphicsScene *m_sharedGraphicScene;
    int m_zOrder;
    static int m_global_max_z;
    int m_max_local_z;
    Node *m_parent;
    QList <Node *> m_children;
    QList <Action *> m_actions;
    QGraphicsItem *m_graphicsItem;

    qreal m_centerX;
    qreal m_centerY;
    qreal m_scaleFactor;
    qreal m_width;
    qreal m_height;
    qreal m_rotaionAngle;
    qreal m_opacityValue;
    int m_nodetag;
    bool m_isTouchEnabled;

};

#endif // CCNODE_H
