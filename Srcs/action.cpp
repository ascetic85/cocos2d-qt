#include "action.h"

Action::Action() {
}

Action* Action::FuncCall(QObject* targeted,const char *slot) {
    Action *newAction = new Action;
    newAction->m_slotCall = slot;
    newAction->m_actionTypeV = actionTypeFuncCall;
    newAction->m_target = targeted;
    return newAction;
}

Action* Action::moveTo(float duration,qreal x,qreal y) {
    Action *newAction = new Action;
    newAction->m_actionTypeV = actionTypeMoveTo;
    newAction->m_endValue[0] = x;
    newAction->m_endValue[1] = y;
    newAction->m_animation.setDuration(1000*duration);
    newAction->m_animation.setPropertyName("pos");
    return newAction;
}

Action* Action::moveBy(float duration,qreal x,qreal y) {
    Action *newAction = new Action;
    newAction->m_actionTypeV = actionTypeMoveBy;
    newAction->m_endValue[0] = x;
    newAction->m_endValue[1] = y;
    newAction->m_animation.setDuration(1000*duration);
    newAction->m_animation.setPropertyName("pos");
    return newAction;

}

Action* Action::scaleTo(float duration,qreal scale) {
    Action *newAction = new Action;
    newAction->m_actionTypeV = actionTypeScaleTo;
    newAction->m_endValue[0] = scale;
    newAction->m_animation.setDuration(1000*duration);
    newAction->m_animation.setPropertyName("scale");
    return newAction;

}

Action* Action::scaleBy(float duration,qreal scale) {
    Action *newAction = new Action;
    newAction->m_actionTypeV = actionTypeScaleBy;
    newAction->m_endValue[0] = scale;
    newAction->m_animation.setDuration(1000*duration);
    newAction->m_animation.setPropertyName("scale");
    return newAction;

}

Action* Action::fadeTo(float duration,qreal value) {
    Action *newAction = new Action;
    newAction->m_actionTypeV = actionTypeFadeTo;
    newAction->m_endValue[0] = value;
    newAction->m_animation.setDuration(1000*duration);
    newAction->m_animation.setPropertyName("opacity");
    return newAction;

}

Action* Action::fadeBy(float duration,qreal value) {
    Action *newAction = new Action;
    newAction->m_actionTypeV = actionTypeFadeBy;
    newAction->m_endValue[0] = value;
    newAction->m_animation.setDuration(1000*duration);
    newAction->m_animation.setPropertyName("opacity");
    return newAction;

}

Action* Action::rotateTo(float duration,qreal angle) {
    Action *newAction = new Action;
    newAction->m_actionTypeV = actionTypeRotateTo;
    newAction->m_endValue[0] = angle;
    newAction->m_animation.setDuration(1000*duration);
    newAction->m_animation.setPropertyName("rotate");
    return newAction;

}

Action* Action::rotateBy(float duration,qreal angle) {
    Action *newAction = new Action;
    newAction->m_actionTypeV = actionTypeRotateBy;
    newAction->m_endValue[0] = angle;
    newAction->m_animation.setDuration(1000*duration);
    newAction->m_animation.setPropertyName("rotate");
    return newAction;

}


Action* Action::easein(Action* withAction,int rate) {
    if (rate < 0) {
        rate = 0;
        qDebug() << "GameEngine :: easing rate should have a range between 0 and 5, range has been set to 0 for you this time";
    }
    if (rate > 5) {
        rate = 5;
        qDebug() << "GameEngine :: easing rate should have a range between 0 and 5, range has been set to 5 for you this time";
    }
    if (rate == 0)
        withAction->m_animation.setEasingCurve(QEasingCurve::Linear);
    if (rate == 1)
        withAction->m_animation.setEasingCurve(QEasingCurve::InQuad);
    if (rate == 2)
        withAction->m_animation.setEasingCurve(QEasingCurve::InCubic);
    if (rate == 3)
        withAction->m_animation.setEasingCurve(QEasingCurve::InQuart);
    if (rate == 4)
        withAction->m_animation.setEasingCurve(QEasingCurve::InCirc);
    if (rate == 5)
        withAction->m_animation.setEasingCurve(QEasingCurve::InBack);

    return withAction;
}

Action* Action::easeout(Action* withAction,int rate) {
    if (rate < 0) {
        rate = 0;
        qDebug() << "GameEngine :: easing rate should have a range between 0 and 5, range has been set to 0 for you this time";
    }
    if (rate > 5) {
        rate = 5;
        qDebug() << "GameEngine :: easing rate should have a range between 0 and 5, range has been set to 5 for you this time";
    }
    if (rate == 0)
        withAction->m_animation.setEasingCurve(QEasingCurve::Linear);
    if (rate == 1)
        withAction->m_animation.setEasingCurve(QEasingCurve::OutQuad);
    if (rate == 2)
        withAction->m_animation.setEasingCurve(QEasingCurve::OutCubic);
    if (rate == 3)
        withAction->m_animation.setEasingCurve(QEasingCurve::OutQuart);
    if (rate == 4)
        withAction->m_animation.setEasingCurve(QEasingCurve::OutCirc);
    if (rate == 5)
        withAction->m_animation.setEasingCurve(QEasingCurve::OutBack);
    return withAction;
}

Action* Action::easeinout(Action* withAction,int rate) {
    if (rate < 0) {
        rate = 0;
        qDebug() << "GameEngine :: easing rate should have a range between 0 and 5, range has been set to 0 for you this time";
    }
    if (rate > 5) {
        rate = 5;
        qDebug() << "GameEngine :: easing rate should have a range between 0 and 5, range has been set to 5 for you this time";
    }
    if (rate == 0)
        withAction->m_animation.setEasingCurve(QEasingCurve::Linear);
    if (rate == 1)
        withAction->m_animation.setEasingCurve(QEasingCurve::InOutQuad);
    if (rate == 2)
        withAction->m_animation.setEasingCurve(QEasingCurve::InOutCubic);
    if (rate == 3)
        withAction->m_animation.setEasingCurve(QEasingCurve::InOutQuart);
    if (rate == 4)
        withAction->m_animation.setEasingCurve(QEasingCurve::InOutCirc);
    if (rate == 5)
        withAction->m_animation.setEasingCurve(QEasingCurve::InOutBack);
    return withAction;
}


void Action::setTarget(Node *targeted) {
    if (m_actionTypeV == actionTypeFuncCall) {
        connect(this,SIGNAL(animationFinished(Action*)),m_target,m_slotCall);
        delete_self();
    }
    else {
    m_target = targeted;
    qreal newY;
    qreal newX;
    switch (m_actionTypeV) {
    case actionTypeMoveTo :
        m_animation.setStartValue(targeted->pos());
        //here we are setting the new position
        m_animation.setEndValue(QPointF(m_endValue[0],m_endValue[1]));
        break;
    case actionTypeMoveBy :
        m_animation.setStartValue(targeted->pos());
        //here we'll calculate the new point as it's moveBy, we'll add the value to the original
        newX = m_endValue[0] + targeted->pos().x();
        newY = m_endValue[1] + targeted->pos().y();
        m_animation.setEndValue(QPointF(newX,newY));
        break;
    case actionTypeFadeTo :
        m_animation.setStartValue(targeted->opacity());
        //here we'll set the new scale
        m_animation.setEndValue(m_endValue[0]);
        break;
    case actionTypeFadeBy :
        m_animation.setStartValue(targeted->opacity());
        //here we'll calculate the new scale as it's multiplied by original scale
        m_animation.setEndValue(m_endValue[0]+targeted->opacity());
        break;
    case actionTypeScaleTo:
        m_animation.setStartValue(targeted->scale());
        //here we'll set the new scale
        m_animation.setEndValue(m_endValue[0]);
        break;
    case actionTypeScaleBy :
        m_animation.setStartValue(targeted->scale());
        //here we'll calculate the new scale as it's multiplied by original scale
        m_animation.setEndValue(m_endValue[0]*targeted->scale());
        break;
    case actionTypeRotateTo :
        m_animation.setStartValue(targeted->rotation());
        //here we'll set the new scale
        m_animation.setEndValue(m_endValue[0]);
        break;
    case actionTypeRotateBy :
        m_animation.setStartValue(targeted->rotation());
        //here we'll calculate the new scale as it's multiplied by original scale
        m_animation.setEndValue(m_endValue[0]+targeted->rotation());
        break;
    default:
        emit animationFinished(this);
        break;
    }
    m_animation.setTargetObject(m_target);
    m_animation.start();
    //here we are connecting it to delete itself slot, and emit done signal
    connect(&m_animation,SIGNAL(finished()),this,SLOT(delete_self()));
}

}


void Action::delete_self() {
        emit animationFinished(this);
}

void Action::stopAnimation() {
    m_animation.stop();
}
