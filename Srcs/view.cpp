#include "view.h"

View::View(QRect deskSize)
{
    m_winSize = deskSize;
}

void View::paintEvent(QPaintEvent *)
{
//    QPaintEvent *newEvent=new QPaintEvent(event->region().boundingRect());
    QPaintEvent *newEvent=new QPaintEvent(m_winSize);
    QGraphicsView::paintEvent(newEvent);
    delete newEvent;
}
