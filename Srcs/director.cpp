#include "director.h"

View* Director::m_view = 0;
Director* Director::m_director = 0;
QMutex Director::m_mutex;
QRect Director::m_desk;
Scene* Director::m_currentscene = 0;

#include <QGraphicsTextItem>
#include "transitionscene.h"
#include <QSettings>

Director::Director() {

}
Director::~Director() {
    pauseView();
}

void Director::setupDirector() {

    if (m_view == 0)
    {
        //save window size
        m_desk.setWidth(QApplication::desktop()->screen()->width());
        m_desk.setHeight(QApplication::desktop()->screen()->height());

        //uncomment this if condition and comment the one under it, if you would like a portraite mode
        //if (desk.height() < desk.width()) {
        if (m_desk.height() > m_desk.width()) {
            int prevH = m_desk.height();
            m_desk.setHeight(m_desk.width());
            m_desk.setWidth(prevH);
        }
        // if it's windows, simulator, check full screen variable, if on mobile phone, run full screen please
#if defined(Q_OS_MAC) || defined(Q_OS_UNIX) || defined(Q_OS_WIN)
    #ifdef fullScreen
        GraphicView = new MyFasterGraphicView(desk);
        GraphicView->showFullScreen();
    #else
        QSettings configs("config.ini",QSettings::IniFormat);
        m_desk.setWidth(configs.value("width", 480).toInt());
        m_desk.setHeight(configs.value("height", 320).toInt());
        m_view = new View(m_desk);
    #endif // fullscreen end

#else   //mobile phones will use full screen no matter
        GraphicView = new MyFasterGraphicView(desk);
        GraphicView->showFullScreen();
#endif // Q_OS_MAC ...... end

        m_view->setStyleSheet("background-color: black");
        m_view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        m_view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        m_view->setOptimizationFlag(QGraphicsView::DontSavePainterState,true);
        //setting Qt::WA_TranslucentBackground improves performance on mobile phones!!,it seems it doesn't help a lot!
        m_view->setAttribute(Qt::WA_TranslucentBackground,false);
        m_view->setFrameStyle(0);
        setOrientation(Director::ScreenOrientationLockLandscape);
//        #ifdef Q_OS_SYMBIAN
        //do nothing, don't turn on advance graphics features
//        #else   //symbian is slow so this will only apply for OSes other than symbian phones, also we don't need these on other devices, if needed, we'll just uncomment this code
//        GraphicView->setRenderHints(QPainter::NonCosmeticDefaultPen | QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform | QPainter::HighQualityAntialiasing);
//        #endif
        m_view->setGeometry(m_desk);
        m_desk.setX(0);
        m_desk.setY(0);
        //set graphics view to screen size
        m_scene = new GraphicsScene;
        m_view->setScene(m_scene);
        m_scene->setItemIndexMethod(QGraphicsScene::NoIndex);
        m_scene->addRect(m_desk,Qt::NoPen,QColor(0,0,0,255))->setZValue(-1000);

        if (displyFrameRate) {
            Director::sharedDirector()->m_frameRateText = m_scene->addText(QString().setNum(60));
            m_frameRateText->setPlainText(QString().setNum(frameRate));
            QFont newfont("Ariel",16);
            newfont.setPixelSize(newfont.pointSize());
            m_frameRateText->setFont(newfont);
            Director::sharedDirector()->m_frameRateText->setZValue(10000);
            Director::sharedDirector()->m_frameRateText->setDefaultTextColor(QColor(255,255,255,150));
            m_frameRateText->setPos(0,m_desk.height()-(m_frameRateText->document()->size().height()));
        }
        m_view->setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
        Director::sharedDirector()->m_delta.restart();
        Director::sharedDirector()->m_frameSchedular.start((1.0/frameRate)*1000.0);
        m_isRuning = true;
        m_view->setSceneRect(m_desk);
        DEBUG_INFO("Game Engine :: Screen size : w: " << m_desk.width() << " x h: " << m_desk.height());

        m_updateLessTimes = frameRate/3;

        #ifdef useOpenGL

        //openGL settings

        QGLFormat format;
        if (format.hasOpenGL()) {
            format.setProfile(QGLFormat::CoreProfile);
            format.setAlpha(openGLAlpha);
            format.setDoubleBuffer(openGLdoubleBuffering);
            format.setDepth(openGLdepthBuffer);

            //set open gl version to max available, they have to be in order from smallest to biggest

            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_ES_CommonLite_Version_1_0) ||
                    format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_ES_Common_Version_1_0) ) {
                format.setVersion(1,0);
            }
            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_1_1) ||
                    format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_ES_CommonLite_Version_1_1) ||
                    format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_ES_Common_Version_1_1)
                    ) {
                format.setVersion(1,1);
            }
            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_1_2)
                    ) {
                format.setVersion(1,2);
            }

            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_1_3)
                    ) {
                format.setVersion(1,3);
            }

            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_1_4)
                    ) {
                format.setVersion(1,4);
            }

            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_1_5)
                    ) {
                format.setVersion(1,5);
            }

            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_2_0) ||
                    format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_ES_Version_2_0)
                    ) {
                format.setVersion(2,0);
            }

            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_2_1)
                    ) {
                format.setVersion(2,1);
            }

            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_3_0)
                    ) {
                format.setVersion(3,0);
            }

            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_3_1)
                    ) {
                format.setVersion(3,1);
            }

            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_3_2)
                    ) {
                format.setVersion(3,2);
            }

            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_3_3)
                    ) {
                format.setVersion(3,3);
            }


            if (format.openGLVersionFlags().testFlag(QGLFormat::OpenGL_Version_4_0)
                    ) {
                format.setVersion(4,0);
            }


            openGLwidget = new QGLWidget(format);
            openGLwidget->setGeometry(desk);
            m_view->setViewport(openGLwidget);
            openGLwidget->setStyleSheet("background-color: black");
            DEBUG_INFO("Game Engine :: OpenGL " <<format.majorVersion() << "." << format.minorVersion());
            //            DEBUG_INFO("Supported OpenGL are " << format.openGLVersionFlags());
        } else {
            DEBUG_INFO("Game Engine :: OpenGL not supported, performance might decrease significatly");
            openGLwidget = 0;
        }
        #else
        m_openGLwidget = 0;
        #endif // useOpenGL end
        //end of opengl setting


#if defined(Q_OS_MAC) || defined(Q_OS_UNIX) || defined(Q_OS_WIN)
            m_view->move((QApplication::desktop()->width() - m_view->width()) / 2
                       , (QApplication::desktop()->height() - m_view->height()) / 2);
#endif
        if (m_openGLwidget == 0) {

            m_view->show();
            connect(&m_frameSchedular,SIGNAL(timeout()),Director::sharedDirector(),SLOT(updatedEveryFrameRate()));
        } else {
#if defined(Q_OS_WIN)
    #ifndef fullScreen
            m_view->showNormal();
    #endif
#endif
            connect(&m_frameSchedular,SIGNAL(timeout()),Director::sharedDirector(),SLOT(updatedEveryFrameRateOpenGL()));
        }
    }
}

void Director::startWithScene(Scene *scene) {
    m_currentscene = scene;
    scene->mainLayer->onEnterTransitionDidStart();
    continueView();
    scene->mainLayer->onEnterTransitionDidFinish();
    return;
}

void Director::replaceScene(Scene *scene) {
    if (m_currentscene != 0 ) {
        m_currentscene->mainLayer->onExitTransitionDidStart();
        m_currentscene->mainLayer->onExitTransitionDidFinish();
        delete m_currentscene;
    }
    scene->mainLayer->onEnterTransitionDidStart();
    continueView();
    m_currentscene = scene;
    m_currentscene->mainLayer->onEnterTransitionDidFinish();
}

void Director::replaceScene(TransitionScene *traSce) {
    traSce->setCurrScene(m_currentscene);
    continueView();
}
void Director::transitionDone(Scene* scene) {
    delete m_currentscene;
    m_currentscene = scene;
}

Director* Director::sharedDirector()
{
    if (!m_director)
    {
        m_mutex.lock();

        if (!m_director)
        {
//            QThread *director_thread = new QThread;
            m_director = new Director;
//            director->moveToThread(director_thread);
//            director_thread->start();
            m_director->setupDirector();
        }
        m_mutex.unlock();
    }

    return m_director;
}

QRect Director::winSizeInPixels() {
    return m_desk;
}

void Director::updatedEveryFrameRate() {
    int elapsed = m_delta.elapsed();
    emit update(elapsed/1000.0);
    if (displyFrameRate) {
        double rate = elapsed;
        rate/= 1000.0;
        rate = 1.0/rate;
        if (rate > frameRate + 10)
            rate = frameRate;
        m_updateLessTimes++;
        if (m_updateLessTimes > (frameRate/2)) {
            m_updateLessTimes = 0;
            m_frameRateText->setPlainText(QString().setNum(rate,'g',3));
        }
    }
    m_delta.start();
//    GraphicView->update(desk);
}
void Director::updatedEveryFrameRateOpenGL() {
    int elapsed = m_delta.elapsed();
    emit update(elapsed/1000.0);
    if (displyFrameRate) {
        double rate = elapsed;
        rate/= 1000.0;
        rate = 1.0/rate;
        if (rate > frameRate + 10)
            rate = frameRate;
        m_updateLessTimes++;
        if (m_updateLessTimes > (frameRate/2)) {
            m_updateLessTimes = 0;
            m_frameRateText->setPlainText(QString().setNum(rate,'g',3));

        }
    }
    m_delta.start();
//    openGLwidget->update(desk);
}


QGraphicsView * Director::sharedGraphicView() {
    return m_view;
}

void Director::pauseView() {
    if (m_isRuning) {
        if (m_openGLwidget == 0) {
            disconnect(&m_frameSchedular,SIGNAL(timeout()),Director::sharedDirector(),SLOT(updatedEveryFrameRate()));
        } else {
            disconnect(&m_frameSchedular,SIGNAL(timeout()),Director::sharedDirector(),SLOT(updatedEveryFrameRateOpenGL()));
        }
        m_isRuning = 0;
    }
}

void Director::continueView() {
    if (!m_isRuning) {
        m_isRuning = 1;
        if (m_openGLwidget == 0) {
            connect(&m_frameSchedular,SIGNAL(timeout()),Director::sharedDirector(),SLOT(updatedEveryFrameRate()));
        } else {
            connect(&m_frameSchedular,SIGNAL(timeout()),Director::sharedDirector(),SLOT(updatedEveryFrameRateOpenGL()));
        }
    }
}

GraphicsScene * Director::sharedGraphicScene() {
    return m_scene;
}

void Director::setOrientation(ScreenOrientation orientation)
{
#if defined(Q_OS_SYMBIAN)
    // If the version of Qt on the device is < 4.7.2, that attribute won't work
    if (orientation != ScreenOrientationAuto) {
        const QStringList v = QString::fromAscii(qVersion()).split(QLatin1Char('.'));
        if (v.count() == 3 && (v.at(0).toInt() << 16 | v.at(1).toInt() << 8 | v.at(2).toInt()) < 0x040702) {
            qWarning("Screen orientation locking only supported with Qt 4.7.2 and above");
            return;
        }
    }
#endif // Q_OS_SYMBIAN

    Qt::WidgetAttribute attribute;
    switch (orientation) {
#if QT_VERSION < 0x040702
    // Qt < 4.7.2 does not yet have the Qt::WA_*Orientation attributes
    case ScreenOrientationLockPortrait:
        attribute = static_cast<Qt::WidgetAttribute>(128);
        break;
    case ScreenOrientationLockLandscape:
        attribute = static_cast<Qt::WidgetAttribute>(129);
        break;
    default:
    case ScreenOrientationAuto:
        attribute = static_cast<Qt::WidgetAttribute>(130);
        break;
#else // QT_VERSION < 0x040702
    case ScreenOrientationLockPortrait:
        attribute = Qt::WA_LockPortraitOrientation;
        break;
    case ScreenOrientationLockLandscape:
        attribute = Qt::WA_LockLandscapeOrientation;
        break;
    default:
    case ScreenOrientationAuto:
        attribute = Qt::WA_AutoOrientation;
        break;
#endif // QT_VERSION < 0x040702
    };
    m_view->setAttribute(attribute, true);
}

QPointF Director::convertTouchPoint(QPointF oldPoint) {
    //we'll do convert the touch point
    return QPointF(oldPoint.x(),m_desk.height() - oldPoint.y());
}
