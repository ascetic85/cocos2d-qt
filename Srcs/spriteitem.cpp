#include "spriteitem.h"
#include <QGraphicsSceneMouseEvent>
#include <QDebug>
#include <QBitmap>
#include "graphicsscene.h"
#include <QTransform>

SpriteItem::SpriteItem(const QString &filename,bool button,const QString &clickedFileName) {
    bool load = QPixmapCache::find(filename,&m_normal);
    if (!load) {
        load = m_normal.load(filename);
        if (load) {
            QPixmapCache::insert(filename,m_normal);
        } else {
            qDebug() << "Game Engine :: File \"" << filename << "\" is not successfuly loaded, make sure file exists";
        }
    }
    if (button) {
        if (clickedFileName == "") {
            if (load) {
                m_clicked.load(filename);
                m_clicked.setMask(m_clicked.createMaskFromColor(QColor(0,0,0,100)));
            }
        }
        else {
            bool load = QPixmapCache::find(clickedFileName,&m_clicked);
            if (!load) {
                load = m_clicked.load(clickedFileName);
                if (load) {
                    QPixmapCache::insert(clickedFileName,m_clicked);
                } else {
                    qDebug() << "Game Engine :: File \"" << clickedFileName << "\" is not successfuly loaded, make sure file exists";
                }
            }
        }
    }
    setPixmap(m_normal);
    setTransformationMode(Qt::SmoothTransformation);
    m_parentMenuItem = 0;
    setFlag(QGraphicsItem::ItemSendsGeometryChanges,0);
    setFlag(QGraphicsItem::ItemIsMovable,0);
    setFlag(QGraphicsItem::ItemIsSelectable,0);
    setFlag(QGraphicsItem::ItemIsFocusable,0);
    setFlag(QGraphicsItem::ItemIgnoresTransformations,0);
    setFlag(QGraphicsItem::ItemIgnoresParentOpacity,0);
    setFlag(QGraphicsItem::ItemAcceptsInputMethod,0);
    setFlag(QGraphicsItem::ItemNegativeZStacksBehindParent,0);
    setEnabled(0);
    setCacheMode(QGraphicsItem::NoCache);
}
SpriteItem::~SpriteItem() {

}

qreal SpriteItem::height() {
    return this->pixmap().height();
}
qreal SpriteItem::width() {
    return this->pixmap().width();
}


void SpriteItem::setParent(MenuItem *parent) {
    m_parentMenuItem = parent;
}

