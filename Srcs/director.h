#ifndef DIRECTOR_H
#define DIRECTOR_H

// this is a singletone class, the director is one and can change the scenes as you wish to
//class Scene;

#include <QMutex>
#include <QTimer>
#include <QTime>
#include <QApplication>
#include <QDesktopWidget>
#include "game_config.h"
#include <QDebug>
#include "graphicsscene.h"
#include <QtOpenGL>
#include "textitem.h"
#include "view.h"

class TransitionScene;
class Scene;

class Director : public QObject
{
    Q_OBJECT

public:
    void setupDirector();
    QGraphicsView* sharedGraphicView();
    GraphicsScene* sharedGraphicScene();
    static Director* sharedDirector();
    QRect winSizeInPixels();
    void startWithScene(Scene *m_scene);
    void replaceScene(Scene *m_scene);
    void replaceScene(TransitionScene *traSce);
    void pauseView();
    void continueView();

    enum ScreenOrientation {
        ScreenOrientationLockPortrait,
        ScreenOrientationLockLandscape,
        ScreenOrientationAuto
    };
    void setOrientation(ScreenOrientation orientation);
    static QPointF convertTouchPoint(QPointF oldPoint);

public slots:
    void updatedEveryFrameRate();
    void updatedEveryFrameRateOpenGL();
    void transitionDone(Scene* m_scene);
signals:
    void update(double deltams);

private:
    Director();	// hide constructor
    ~Director();	// hide destructor
    Director(const Director &); // hide copy constructor
    Director& operator=(const Director &); // hide assign op

    static Scene *m_currentscene;
    static Director *m_director;
    static QMutex m_mutex;
    static QRect m_desk;

    QGraphicsTextItem *m_frameRateText;
    QTime m_delta;
    QTimer m_frameSchedular;
    static View *m_view;
    bool m_isRuning;
    GraphicsScene *m_scene;
    QGLWidget* m_openGLwidget;
    int m_updateLessTimes;

};

#endif // SHAREDDIRECTOR_H
