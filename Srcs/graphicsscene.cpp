#include "graphicsscene.h"
#include "QDebug"
#include <QGraphicsSceneMouseEvent>
#include "director.h"

GraphicsScene::GraphicsScene(QObject *parent) :
    QGraphicsScene(parent)
{
    m_mouseDown = 0;
    setSceneRect(0,0,Director::sharedDirector()->winSizeInPixels().width(),Director::sharedDirector()->winSizeInPixels().height());
}

void GraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    emit signaltouchpressed(event);
    m_mouseDown = 1;
}
void GraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event) {
    if (m_mouseDown) emit signaltouchmoved(event);
}
void GraphicsScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event) {
    emit signaltouchreleased(event);
    m_mouseDown = 0;
}
