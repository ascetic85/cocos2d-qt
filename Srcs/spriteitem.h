#ifndef SPRITEITEM_H
#define SPRITEITEM_H
#include <QEvent>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QPixmapCache>
#include "menuitem.h"

class SpriteItem : public QGraphicsPixmapItem
{
public:
    SpriteItem(const QString &filename, bool button, const QString &clickedFileName);
    ~SpriteItem();
    qreal width();
    qreal height();
    void setClickedImage(){ setPixmap(m_clicked);}
    void setNormalImage(){ setPixmap(m_normal); }
    void setParent(MenuItem* parent);

private:
    MenuItem *m_parentMenuItem;
    QPixmap m_normal;
    QPixmap m_clicked;

};

#endif // SPRITEITEM_H
