#ifndef ACTION_H
#define ACTION_H
#include <QPropertyAnimation>
#include "node.h"

class Node;

class Action : public QObject
{
    Q_OBJECT

public:

    static Action* FuncCall(QObject* targeted,const char* slot);
    static Action* moveTo(float duration,qreal x,qreal y);
    static Action* moveBy(float duration,qreal x,qreal y);
    static Action* scaleTo(float duration,qreal scale);
    static Action* scaleBy(float duration,qreal scale);
    static Action* fadeTo(float duration,qreal value);
    static Action* fadeBy(float duration,qreal value);
    static Action* rotateTo(float duration,qreal angle);
    static Action* rotateBy(float duration,qreal angle);
    static Action* easein(Action* withAction,int rate = 4);
    static Action* easeout(Action* withAction,int rate = 4);
    static Action* easeinout(Action* withAction,int rate = 4);
    void setTarget(Node *targeteted);
    void stopAnimation();

private:
    enum actionType {
        actionTypeMoveTo = 0,
        actionTypeMoveBy,
        actionTypeFadeTo,
        actionTypeFadeBy,
        actionTypeScaleTo,
        actionTypeScaleBy,
        actionTypeRotateTo,
        actionTypeRotateBy,
        actionTypeFuncCall
    };

    Action();

    QObject *m_target;
    //this is the animation variable
    QPropertyAnimation m_animation;
    actionType m_actionTypeV;
    qreal m_endValue[2];
    const char* m_slotCall;
public slots:
    void delete_self();
signals:
    void animationFinished(Action *finishedAction);
};

#endif // ACTION_H
