#include "sprite.h"

Sprite::Sprite(const QString &filename) {
    item = new SpriteItem(filename,0,"");
    Node::setGraphicsItem(item);
    Node::setW(item->width());
    Node::setH(item->height());
    setPos(0,0);
}

Sprite::~Sprite() {
    //delete item;
}

qreal Sprite::getwidth() {
    return item->width();
}

qreal Sprite::getheight() {
    return item->height();
}

Sprite* Sprite::spriteWithImage(const char* filename) {
    Sprite* newsprite = new Sprite(QString(filename));
    return newsprite;
}



