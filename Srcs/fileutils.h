#ifndef FILEUTILS_H
#define FILEUTILS_H

#include <QString>

class FileUtils
{
public:
    static FileUtils * shareFileUtils();

    /**
    @brief  Set the resource directory; we will find resources relative to this directory.
    @param pszDirectoryName  Relative path to root.
    */
    void setResourceDirectory(const QString &directoryName);

    /**
    @brief  Get the resource directory
    */
    QString & getResourceDirectory();

protected:
    FileUtils();

protected:
    QString m_Directory;
};

#endif // FILEUTILS_H
