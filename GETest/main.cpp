#include <QtGui/QApplication>
#include <QDir>
//here's the first one to change
#include <game_engine.h>
#include "main_menu.h"


int main(int argc, char *argv[])
{
#ifdef useOpenGL
//    QApplication::setGraphicsSystem("opengl");
#else
    QApplication::setGraphicsSystem("raster");
#endif
    QApplication app(argc, argv);

    FileUtils::shareFileUtils()->setResourceDirectory("resource");

    //set up audio playback
    AudioManager::setUpAudioManager();

    //here is the second main_menu to change
    Director::sharedDirector()->startWithScene(MainMenu::scene());

    return app.exec();
}
