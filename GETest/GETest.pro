#-------------------------------------------------
#
# Project created by QtCreator 2012-12-18T15:48:41
#
#-------------------------------------------------

QT          +=  multimedia core gui opengl
CONFIG      +=  mobility
MOBILITY    += systeminfo \
                multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GETest
TEMPLATE = app

INCLUDEPATH += ../Srcs

CONFIG(debug,debug|release): LIBS += -L../debug -llibGE
CONFIG(release,debug|release): LIBS += -L../release -llibGE

SOURCES += main.cpp \
    main_menu.cpp

HEADERS += \
    main_menu.h
