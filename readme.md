cocos2d qt clone
====================

It know as [GameEngine][9] create by [q8phantom][10].

And one post is here [Cocos2d on Qt][11].

This is a first, early version of a game engine/ framework that should keep 
the gap as narrow as possible to implement cocos2d in Qt, cocos2d is an open 
source powerfull openGL game engine available on iOS, Android, webOS, win32 
and now BlackBerry?. Why not work to get it on Qt also! Where in Qt, you will 
write your game once, and then you can build it with exactly the same code on 
iOS, Android and Nokia phones as well as all desktop platforms!, Help is much 
appreciated ;) source code with an example is available!!

Example game on youtube made with an early version of this engine.
[Demo in youtube][8]

For now this is based on QGraphicsView, even though this might change in the near future

It does not want to replace the cocos2d-x or cocos2d-iphone, just want to make
a editor for then.

cocos2d-x 
==================

[cocos2d-x][1] is a multi-platform 2D game engine in C++, based on [cocos2d-iphone][2] and licensed under MIT.
Now this engine has been expanded to iOS, Android, Bada, BlackBerry, Marmalade and desktop operating systems like Linux, WindowsXP & Windows7. 

Multi Platform
-------------
   * iOS:  stable, well tested on iOS 4.x ~ 5.x SDK.
   * Android: stable, well tested on 2.0~4.0, based on ndk r5 ~ r8.
   * Bada: stable on Bada SDK 1.0 & 2.0
   * BlackBerry Playbook & BB10: stable, contribued by engineers working at RIM
   * Marmalade: stable since cocos2d-x-0.11.0
   * Windows: stable, tested on WinXP, Vista, Win7. Please upgrde the drive of your video card if you meet problems on OpenGL functions
   * Linux: usable.

Document
------------------
   * Website: [www.cocos2d-x.org][3]
   * [Online API References][4] 
	
Contact us
------------------
   * Forum: [http://forum.cocos2d-x.org][5]
   * Twitter: [http://www.twitter.com/cocos2dx][6]
   * Sina mini-blog: [http://t.sina.com.cn/cocos2dx][7]
   
[1]: http://www.cocos2d-x.org "cocos2d-x"
[2]: http://www.cocos2d-iphone.org "cocos2d for iPhone"
[3]: http://www.cocos2d-x.org "www.cocos2d-x.org"
[4]: http://www.cocos2d-x.org/projects/cocos2d-x/wiki/Reference "API References"
[5]: http://forum.cocos2d-x.org "http://forum.cocos2d-x.org"
[6]: http://www.twitter.com/cocos2dx "http://www.twitter.com/cocos2dx"
[7]: http://t.sina.com.cn/cocos2dx "http://t.sina.com.cn/cocos2dx"
[8]: http://www.youtube.com/watch?v=ul5--c2T8qk "http://www.youtube.com/watch?v=ul5--c2T8qk"
[9]: http://projects.developer.nokia.com/cocos2d "cocos2d for Qt"
[10]: http://www.developer.nokia.com/Profile/?u=q8phantom "q8phantom"
[11]: http://www.cocos2d-x.org/boards/6/topics/8043?page=1 "http://www.cocos2d-x.org/boards/6/topics/8043?page=1"
